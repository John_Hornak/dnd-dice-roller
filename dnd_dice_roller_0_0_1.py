import random
while True:
    print('1 for d20, 2 for d12, 3 for d10, 4 for d8, 5 for d6, 6 for d4, 7 for d3, 8 for d2')
    print('strike desired key now')
    print('----------------------------------------------------------')
    # this gets the input from user to decide what to roll
    dice = int(input())
    # 3 lines below are for readability
    print('----------------------------------------------------------')
    print('rolling...')
    print('----------------------------------------------------------')
    # rolling based on the option chosen by user
    if dice == 1:
        result = random.randint(1, 20)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 2:
        result = random.randint(1, 12)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 3:
        result = random.randint(1, 10)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 4:
        result = random.randint(1, 8)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 5:
        result = random.randint(1, 6)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 6:
        result = random.randint(1, 4)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 7:
        result = random.randint(1, 3)
        print('rolled', result)
        print('----------------------------------------------------------')
    elif dice == 8:
        result = random.randint(1, 2)
        print('rolled', result)
        print('----------------------------------------------------------')
    # this will break if any other input besides 1-8 is input, if a letter it
    # throws an error, not sure how to fix that
    else:
        print('Error, exiting')
        break
